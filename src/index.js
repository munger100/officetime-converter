const fs = require("fs-extra");
const moment = require("moment");
const R =  require("ramda");
const os = require("os");

const formatDate = date => moment(date).format("DD/MM/YYYY");

const formatTime = time => moment(time).format("H:MM A");

const formatDuration = duration =>
  `${Math.floor(duration.asHours())}:${duration
    .minutes()
    .toString()
    .padStart(2, "0")}`;

(async () => {
  const [
    ,
    ,
    filePath,
    startRaw,
    endRaw,
    username = os.userInfo().username
  ] = process.argv;
  const data = await fs.readJson(filePath);

  const start = moment(startRaw).startOf("day");
  const end = moment(endRaw).endOf("day");
  const entries = R.pipe(
    R.values,
    R.filter(entry => moment(entry.start).isBetween(start, end)),
    R.filter(R.prop("end")),
    R.reverse
  )(data.entries);
  const {
    projects,
    categories,
    organizationProjects,
    organizationCategories
  } = data;

  const header = `Multiple Projects: ${formatDate(start)} to ${formatDate(end)}`;
  const rows = [header];
  let totalDuration = 0;
  entries.forEach(entry => {
    const projectId = entry.project;
    const categoryId = entry.category;

    const project =
      projects[projectId] && !projects[projectId].organization
        ? projects[projectId]
        : organizationProjects[projectId];
    const projectName = R.prop("name", project);

    const category =
      categories[categoryId] && !categories[categoryId].organization
        ? categories[categoryId]
        : organizationCategories[categoryId];
    const categoryName = R.prop("name", category);

    if (!projectName || !categoryName) {
      return;
    }

    const duration = moment(entry.end).diff(moment(entry.start));
    totalDuration += duration;

    const row = [
      "",
      projectName,
      username,
      formatDate(entry.start),
      formatTime(entry.start),
      formatDuration(moment.duration(duration)),
      "0.",
      categoryName,
      ""
    ];
    rows.push(row.join("\t"));
  });
  console.log(rows.join("\n"));
})();
