# How to use

`node src/index.js ~/your_firetime_export.json 01/08/2018 31/08/2018`

```
node = js interpreter
src/index.js = file to run
~/your_firetime_export.json = firetime export file
other arguments: date start/end, **MUST COVER ONE ENTIRE MONTH, NO MORE NO LESS**, use format DD/MM/YYYY
```

What is outputted to command line should be copied, **including the trailing tab,** and pasted in a txt file which can be uploaded to the timesheet-dropbox